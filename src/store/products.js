import {observable} from "mobx";

export class Products {
  @observable products = [];

  constructor(context) {
    this.http = context.http;
  }

  async get() {
    this.products = await this.http.get('/products');
  }

  async create(data) {
    await this.http.post('/products', data);
    await this.get();
  }

  async edit(id, data) {
    await this.http.put('/products/' + id, data);
    await this.get();
  }

  async delete(id) {
    await this.http.delete('/products/' + id);
    await this.get();
  }
}
