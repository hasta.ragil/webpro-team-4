import {action, computed, observable} from "mobx";
import {Http} from "./http";
import {VisitorCount} from "./visitor_count";
import {Products} from "./products";
import {Guestbook} from "./guestbook";
// import {DateTime} from 'luxon';

const getCurrentTime = () => Math.floor(new Date().getTime() / 1000);


export class Store {
  http = new Http();
  @observable userType = 'user';
  @observable loginFailedCount = 0;
  @observable secondCounter = 0;
  @observable loginBlockedUntil = 0;

  visitor_count = new VisitorCount(this);
  products = new Products(this);
  guestbook = new Guestbook(this);

  constructor(props) {
    const loginBlockedUntil = localStorage.getItem('loginBlockedUntil') || 0;
    const loginFailedCount = localStorage.getItem('loginFailedCount') || 0;
    const userType = localStorage.getItem('userType') || 'user';

    this.loginBlockedUntil = loginBlockedUntil;
    this.userType = userType;
    this.loginFailedCount = loginFailedCount;

    setInterval(() => {
      this.secondCounter = getCurrentTime();
    });
    (async () => {
      await this.visitor_count.create({});
      await this.visitor_count.get();
    })();
  }

  @computed
  get blockedTimeRemaining() {
    console.log(this.loginBlockedUntil, this.secondCounter, 'this.secondCounter');
    return Math.max(this.loginBlockedUntil - this.secondCounter, 0);
  }

  @computed
  get isLoginBlocked() {
    return this.blockedTimeRemaining !== 0;
  }

  @action
  setLoginFailedCount(count) {
    this.loginFailedCount = count;
    localStorage.setItem('loginFailedCount', count);
  }

  @action
  setUserType(userType) {
    this.userType = userType;
    localStorage.setItem('userType', userType);
  }

  @action
  setLoginBlockedUntil(loginBlockedUntil) {
    this.loginBlockedUntil = loginBlockedUntil;
    localStorage.setItem('loginBlockedUntil', loginBlockedUntil);
    this.setLoginFailedCount(0);
  }

  @action
  login(username, password) {
    if(this.isLoginBlocked) {
      alert(`Too much failed login attempt. Please try again in ${this.blockedTimeRemaining} second`);
      return false;
    }
    if(username === 'team4' && password === 'webpro') {
      alert('Login success');
      this.setLoginFailedCount(0);
      this.setLoginBlockedUntil(0);
      this.setUserType('admin');
      return true;
    }
    this.setLoginFailedCount(this.loginFailedCount + 1);
    if(this.loginFailedCount >= 3) {
      this.setLoginBlockedUntil(getCurrentTime() + 30);
    }

    alert('Invalid username/password');
    return false;
  }

  @action
  logout() {
    this.setUserType('user');
  }
}
