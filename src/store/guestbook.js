import {observable} from "mobx";

export class Guestbook {
  @observable guestbooks = [];

  constructor(context) {
    this.http = context.http;
  }

  async get() {
    this.guestbooks = await this.http.get('/guestbook');
  }

  async create(data) {
    await this.http.post('/guestbook', data);
    await this.get();
  }

  async del(id) {
    await this.http.delete('/guestbook/' + id);
    await this.get();
  }
}
