import {config} from "../config/app";


export class Http {
  token = '';
  baseUrl = '';

  constructor() {
    this.baseUrl = config.baseUrl;
  }

  setToken(token) {
    this.token = token;
  }

  checkStatus(result) {
    if (!result.ok) {
      return result.json().then(data => {
        return Promise.reject(data);
      });
    }
    return result;
  }

  raw(url, options={}) {
    let headers = new Headers();
    console.log(url, options, "raw http request");
    return fetch(url, {headers: {}})
      .then(response => {
        if (options.raw){
          return response;
        }

        return response.json();
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
  }

  async checkToken() {
    const {token} = this;
    return this.token;
  }

  async get(url, options={}) {
    await this.checkToken();
    let headers = new Headers();
    // headers.append("Authorization", `Bearer ${this.token}`);
    const baseUrl = this.baseUrl;
    const fullUrl = baseUrl + url;
    return fetch(fullUrl, Object.assign(options, {headers}))
      .then(this.checkStatus)
      .then(response => {
        if (options.raw){
          return response;
        }

        return response.json();
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
  }

  async post(url, data, options={}) {
    await this.checkToken();
    let headers = new Headers();
    // headers.append("Authorization", `Bearer ${this.token}`);
    headers.append("Content-Type", "application/json");

    return fetch(this.baseUrl + url, Object.assign({
      method: 'POST',
      headers,
      body: JSON.stringify(data)
    }, options))
      .then(this.checkStatus)
      .then(response => {
        if (options.raw){
          return response;
        }

        return response.json()
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
  }

  async put(url, data, options={}) {
    await this.checkToken();
    let headers = new Headers();
    headers.append("Authorization", `Bearer ${this.token}`);
    headers.append("Content-Type", "application/json");

    return fetch(this.baseUrl + url, Object.assign({
      method: 'PUT',
      headers,
      body: JSON.stringify(data)
    }, options))
      .then(this.checkStatus)
      .then(response => {
        if (options.raw){
          return response;
        }

        return response.json()
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
  }

  async delete(url, options={}) {
    await this.checkToken();
    let headers = new Headers();
    headers.append("Authorization", `Bearer ${this.token}`);

    return fetch(this.baseUrl + url, {
      headers,
      method: 'DELETE'
    })
      .then(this.checkStatus)
      .then(response => {
        if (options.raw){
          return response;
        }

        return response.json()
      })
      .catch(err => {
        console.log(err);
        throw err;
      })
  }

  async upload(file, options={}) {
    await this.checkToken();
    let headers = new Headers();
    headers.append("Authorization", `Bearer ${this.token}`);
    let data = new FormData();
    data.append('file', file);

    return fetch(this.baseUrl + 'v1/files', Object.assign({
      method: 'POST',
      headers,
      body: data
    }, options))
      .then(this.checkStatus)
      .then(response => {
        if (options.raw){
          return response;
        }

        return response.json()
      })
      .catch(err => {
        console.log(err, 'error');
        throw err;
      })
  }
}
