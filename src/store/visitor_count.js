import {observable} from "mobx";

export class VisitorCount {
  @observable count = 0;

  constructor(context) {
    this.http = context.http;
  }

  async get() {
    const result = await this.http.get('/visitor_count');
    this.count = result.count;
  }

  async create(data) {
    await this.http.post('/visitor_count', data);
    await this.get();
  }
}
