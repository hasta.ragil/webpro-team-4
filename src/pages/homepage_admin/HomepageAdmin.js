import React from "react";
import { inject, observer } from "mobx-react";
import { Button, Card, Col, Icon, Layout, Row, Table, List, Select } from 'antd';


@inject("store")
@observer
class HomepageAdmin extends React.Component {

    state = {
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    async componentDidMount() {
        this.store.products.get();
        this.store.visitor_count.get();
        this.store.guestbook.get();
    }

    render() {
        return <div>
            <div style={{ marginBottom: 30, marginTop: 60, width: 'calc(100vw - 200px)', marginLeft: 'auto', marginRight: 'auto' }}>
                <Row gutter={16}>
                    <Col span={6}>
                        <Card style={{ borderRadius: 5, backgroundColor:'#40a9ff' }}>
                            <p style={{ color: 'white', fontSize: '1.2em' }}>Product Count</p>
                            <p style={{ color: 'white', fontSize: '1.7em' }}>{this.store.products.products.length}</p>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card style={{ borderRadius: 5, backgroundColor:'#9C27B0' }}>
                            <p style={{ color: 'white', fontSize: '1.2em' }}>Visitor Count</p>
                            <p style={{ color: 'white', fontSize: '1.7em' }}>{this.store.visitor_count.count}</p>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card style={{ borderRadius: 5, backgroundColor:'#F44336' }}>
                            <p style={{ color: 'white', fontSize: '1.2em' }}>Guestbook Count</p>
                            <p style={{ color: 'white', fontSize: '1.7em' }}>{this.store.guestbook.guestbooks.length}</p>
                        </Card>
                    </Col>
                </Row>
            </div>
        </div>;
    }
}

export {HomepageAdmin}
