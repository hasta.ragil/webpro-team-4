import React from "react";
import { inject, observer } from "mobx-react";
import './reset_password.css';
import ReCAPTCHA from "react-google-recaptcha";
import {withRouter} from "react-router-dom";

@withRouter
@inject("store")
@observer
class ResetPassword extends React.Component {

    state = {
      code: '',
      confirmPassword: '',
      password: ''
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    async componentDidMount() {
    }

    render() {
      return <div>
        <section className="reset_password container-fluid" id="login">
          <h2>Reset Password</h2>
          <form action="">
            <p>
              <label htmlFor="">Masukan kode 6 digit dari email</label> <br/>
              <input type="text" value={this.state.code} onChange={(e) => this.setState({code: e.target.value})}/>
            </p>
            <p>
              <label htmlFor="">New Password</label> <br/>
              <input type="password" value={this.state.password} onChange={(e) => this.setState({password: e.target.value})}/>
            </p>
            <p>
              <label htmlFor="">Confirm New Password</label> <br/>
              <input type="password" value={this.state.confirmPassword} onChange={(e) => this.setState({confirmPassword: e.target.value})}/>
            </p>
            <p>
              <button type="submit" className="btn-submit" onClick={(e) => {
                e.preventDefault();
                if(!this.state.code) {
                  alert('Please input code');
                  return;
                }
                if(!this.state.password) {
                  alert('Please input password');
                  return;
                }
                if(!this.state.confirmPassword) {
                  alert('Please input confirm password');
                  return;
                }
                if(this.state.password !== this.state.confirmPassword) {
                  alert('Password confirmation did\'nt match');
                  return;
                }
                alert('Reset password success, back to home');

                this.props.history.push('/');
              }}>Reset Password</button>
            </p>
          </form>
        </section>
      </div>;
    }
}

export {ResetPassword}
