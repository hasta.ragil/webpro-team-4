import React from "react";
import { inject, observer } from "mobx-react";
import './forgot_password.css';
import {withRouter} from "react-router-dom";


@withRouter
@inject("store")
@observer
class ForgotPassword extends React.Component {

    state = {
      username: ''
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    async componentDidMount() {
    }

    render() {
      return <div>
        <section className="forgot_password container-fluid" id="login">
          <h2>Reset Password</h2>
          <form action="">
            <p>
              <label htmlFor="">Username</label> <br/>
              <input type="text" value={this.state.username} onChange={(e) => this.setState({username: e.target.value})}/>
            </p>
            <p>
              <button type="submit" className="btn-submit" onClick={(e) => {
                e.preventDefault();
                if(!this.state.username) {
                  alert('Please input username');
                  return;
                }
                if(this.state.username !== 'team4') {
                  alert('Username not found');
                  return;
                }
                alert('We have sent 6 digit code, to your email. Please enter it on the next page');

                this.props.history.push('/reset_password');
              }}>Reset Password</button>
            </p>
          </form>
        </section>
      </div>;

    }
}

export {ForgotPassword}
