import React from "react";
import { inject, observer } from "mobx-react";
import { DatePicker, Row, Col, Button, Table, Modal, Form, Input, notification } from 'antd';

const {confirm} = Modal;

@Form.create({ name: 'product' })
class ProductForm extends React.Component {
  constructor(props) {
    super(props);

    console.log('3221');
  }
  componentDidMount() {
    console.log('322');
    console.log(this.props.formRef, 'this.props.formRef');
    if(typeof this.props.formRef === 'function') {
      this.props.formRef(this.props.form);
    }
  }

  render() {
    console.log('32213');
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    return <div>
      <Form {...formItemLayout} onSubmit={() => {
        // this.props.form.validateFields((err, values) => {
        //   if (!err) {
        //     console.log('Received values of form: ', values);
        //   }
        // });
        // console.log('onsubmit');
      }}>
        <Form.Item label="Name">
          {getFieldDecorator('name', {
            initialValue: this.props.isEdit ? this.props.initialValue && this.props.initialValue.name : '',
            rules: [
              {
                required: true,
                message: 'Please input name',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Youtube Link">
          {getFieldDecorator('youtube_link', {
            initialValue: this.props.isEdit ? this.props.initialValue && this.props.initialValue.youtube_link : '',
            rules: [
              {
                required: true,
                message: 'Please input Youtube Link',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Description">
          {getFieldDecorator('description', {
            initialValue: this.props.isEdit ? this.props.initialValue && this.props.initialValue.description : '',
            rules: [
              {
                required: true,
                message: 'Description',
              },
            ],
          })(<Input />)}
        </Form.Item>
      </Form>

    </div>
  }
}


@inject("store")
@observer
class ProductAdmin extends React.Component {

    state = {
      modalVisible: false,
      isEdit: false,
      initialValue: {
        id: '',
        name: '',
        youtube_link: '',
        description: '',
      }
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    componentDidMount() {
      this.props.store.products.get();
    }

    render() {
      const dataSource = [
        {
          key: '1',
          name: 'Integration and the fundamental theorem of calculus',
          youtube_link: 'https://www.youtube.com/watch?v=rfG8ce4nNh0',
          description: '-',
        },
        {
          key: '2',
          name: 'Limits, L\'Hopital\'s rule, and epsilon delta definitions',
          youtube_link: 'https://www.youtube.com/watch?v=kfF40MiS7zA',
          description: '-',
        },
      ];

      const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Youtube Link',
          dataIndex: 'youtube_link',
          key: 'youtube_link',
        },
        {
          title: 'Description',
          dataIndex: 'description',
          key: 'description',
        },
        {
          title: 'Action',
          dataIndex: 'action',
          key: 'action',
          render: (_, row) => {
            return <div style={{
              display: 'flex',
              flexDirection: 'row',
            }}>
              <Button type="primary" style={{marginRight: 8}} onClick={() => {
                this.setState({
                  modalVisible: true,
                  isEdit: true,
                  initialValue: {
                    id: row.id,
                    name: row.name,
                    youtube_link: row.youtube_link,
                    description: row.description,
                  },
                });
              }}>Edit</Button>
              <Button type="danger" onClick={() => {
                confirm({
                  title: 'Do you want to delete the data?',
                  onOk: () => {
                    this.props.store.products.delete(row.id).then(it => {
                      notification.success({
                        message: 'Delete data success'
                      });
                    });
                    console.log('OK');
                  },
                  onCancel: () => {
                    console.log('Cancel');
                  },
                });

                // this.props.store.guestbook
              }}>Delete</Button>
            </div>
          }
        },
      ];

        return <div style={{
          width: '80vw',
          marginLeft: 'auto',
          marginRight: 'auto',
          display: 'flex',
          flexDirection: 'column',
          marginTop: 96
        }}>
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end'
          }}>
            <Button type="primary" onClick={() => {
              this.setState({
                isEdit: false,
                initialValue: {
                  id: '',
                  name: '',
                  youtube_link: '',
                  description: '',
                },
                modalVisible: true
              })
            }}>Create</Button>
          </div>
          <div style={{
            marginTop: 16
          }}>
            <Table dataSource={this.props.store.products.products} columns={columns} />
          </div>

          <Modal
            title={this.state.isEdit ? "Edit" : "Create"}
            visible={this.state.modalVisible}
            onOk={() => {
              this.form.validateFields((err, values) => {
                if(err) {
                  console.log(err, 'err');
                }
                if (!err) {
                  console.log('Received values of form: ', values);
                  if(this.state.isEdit) {
                    this.props.store.products.edit(this.state.initialValue.id, values).then(() => {
                      this.setState({
                        modalVisible: false
                      });
                    });
                  } else {
                    this.props.store.products.create(values).then(() => {
                      this.setState({
                        modalVisible: false
                      });
                    });
                  }
                }
              });
            }}
            onCancel={() => {
              this.setState({
                modalVisible: false
              })
            }}
          >
            <ProductForm formRef={(form) => {
              this.form = form;
            }} initialValue={this.state.initialValue} isEdit={this.state.isEdit}/>
          </Modal>
        </div>;
    }
}

export {ProductAdmin}
