import React from "react";
// import { inject, observer } from "mobx-react";
// import { Store } from "../../store";
import { Modal, Button, notification } from 'antd';
import {inject, observer} from "mobx-react";

const { confirm } = Modal;

const team = [{
  name: 'Aden Rahmandi',
  nim: '2101703783',
  image: require('../../assets/image/team/aden.jpeg')
}, {
  name: 'Ajeng Setoresmi',
  nim: '2101721861',
  image: require('../../assets/image/team/ajeng.jpeg')
}, {
  name: 'Aqfin Elmiyansa',
  nim: '2101721344',
  image: require('../../assets/image/team/aqfin.jpeg')
}, {
  name: 'Hafidz Azzikri',
  nim: '2101715322',
  image: require('../../assets/image/team/hafidz.jpeg')
}, {
  name: 'Hasta Ragil Saputra',
  nim: '2101718835',
  image: require('../../assets/image/team/hasta_ragil_saputra.jpeg')
}];

@inject("store")
@observer
class Homepage extends React.Component {

  state = {};

  constructor(props) {
    super(props);
    // this.store = this.props.store;
  }

  async componentDidMount() {
    this.props.store.products.get();
  }

  render() {
    return <div>
      <div className="banner">

        <div className="title">
          <h1>Belajar lebih mudah bersama kami</h1>
          <h5>Dimanapun kamu berada tetap bisa belajar dimanapun dan kapapun belajar apapun disini dan raih
            kesuksesan secepat mungkin</h5>
        </div>
      </div>

      <section className="product container-fluid" id="product">
        <h2>product tersedia</h2>
        <div className="wrapper">
          {this.props.store.products.products.map(it => {
            const youtubeEmbedLink = (new URL(it.youtube_link)).searchParams.get('v');
            return (<div className="item">
              <div className="image-hero">
                <iframe src={"https://www.youtube.com/embed/" + youtubeEmbedLink} frameBorder="0"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen/>
                {/*<img src={require('../../assets/image/learn_web.png')} alt=""/>*/}
              </div>
              <div className="text">
                <span className="tingkat">pemula</span>
                <span className="durasi">30 Hari</span>
                <h4>{it.name}</h4>
                <p>{it.description}</p>

                <button className="btn-primary">daftar kelas</button>
              </div>
            </div>)
          })}
        </div>
      </section>

      <section className="our-team container-fluid" id="our-team">
        <h2>Our Team</h2>
        <div style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center'
        }}>
          {team.map(it => {
            return <div style={{
              display: 'flex',
              flexDirection: 'column',
              margin: 10,
              alignItems: 'center'
            }}>
              <img src={it.image} alt="" style={{
                width: 130,
                borderRadius: 50
              }}/>
              <h3 style={{
                marginTop: 10
              }}>{it.name}</h3>
              <h4 style={{
                fontWeight: 100,
                marginTop: 10
              }}>{it.nim}</h4>
            </div>
          })}
        </div>
        {/*<div className="team-all">*/}
        {/*  <div className="team">*/}
        {/*    <div className="image-hero">*/}
        {/*      <img src={require('../../assets/image/team/aden.jpeg')} alt=""/>*/}
        {/*    </div>*/}
        {/*    <div className="data">*/}
        {/*      <h3>Aden Rahmandi</h3>*/}
        {/*      <h4>2101703783</h4>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*  <div className="team">*/}
        {/*    <div className="image-hero">*/}
        {/*      <img src={require('../../assets/image/team/ajeng.jpeg')} alt=""/>*/}
        {/*    </div>*/}
        {/*    <div className="data">*/}
        {/*      <h3>ajeng setoresmi</h3>*/}
        {/*      <h4>2101721861</h4>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*  <div className="team">*/}
        {/*    <div className="image-hero">*/}
        {/*      <img src={require('../../assets/image/team/aqfin.jpeg')} alt=""/>*/}
        {/*    </div>*/}
        {/*    <div className="data">*/}
        {/*      <h3>Aqfin Elmiyansa</h3>*/}
        {/*      <h4>2101721344</h4>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*  <div className="team">*/}
        {/*    <div className="image-hero">*/}
        {/*      <img src={require('../../assets/image/team/hafidz.jpeg')} alt=""/>*/}
        {/*    </div>*/}
        {/*    <div className="data">*/}
        {/*      <h3>Hafidz Azzikri</h3>*/}
        {/*      <h4>2101715322</h4>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*  <div className="team">*/}
        {/*    <div className="image-hero">*/}
        {/*      <img src={require('../../assets/image/team/hasta_ragil_saputra.jpeg')} alt=""/>*/}
        {/*    </div>*/}
        {/*    <div className="data">*/}
        {/*      <h3>Hasta Ragil Saputra</h3>*/}
        {/*      <h4>2101718835</h4>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*</div>*/}
      </section>

      <section className="contact container-fluid" id="contact">
        <h2>Contact</h2>
        <form action="" onSubmit={(e) => {
          e.preventDefault();
          confirm({
            title: 'Do you want to submit guestbook?',
            onOk: () => {
              this.props.store.guestbook.create(this.state).then(it => {
                notification.success({
                  message: 'Create guestbook success'
                });
                this.setState({
                  fullname: '',
                  email: '',
                  message: '',
                });
              });
              console.log('OK');
            },
            onCancel: () => {
              console.log('Cancel');
            },
          });
        }}>
          <p>
            <label htmlFor="">Nama Lengkap</label> <br/>
            <input type="text" placeholder="Masukan Nama Anda" value={this.state.fullname} onChange={(e) => this.setState({
              fullname: e.target.value
            })}/>
          </p>
          <p>
            <label htmlFor="">Email</label> <br/>
            <input type="text" placeholder="Masukan Email Anda" value={this.state.email} onChange={(e) => this.setState({
              email: e.target.value
            })}/>
          </p>
          <p>
            <label htmlFor="">Pesan</label> <br/>
            <textarea cols="30" rows="10" placeholder="Masukan Pesan Anda" value={this.state.message} onChange={(e) => this.setState({
              message: e.target.value
            })}></textarea>
          </p>
          <p>
            <button type="submit" className="btn-submit">Kirim Pesan</button>
          </p>
        </form>
      </section>
    </div>;
  }
}
export {Homepage}
