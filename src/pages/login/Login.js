import React from "react";
import ReCAPTCHA from "react-google-recaptcha";
import './login.css';
import {inject, observer} from "mobx-react";
import {
  withRouter
} from 'react-router-dom';

@withRouter
@inject("store")
@observer
class Login extends React.Component {

  state = {
    validated: false,
    username: '',
    password: '',
  };

  constructor(props) {
    super(props);
    this.store = this.props.store;

  }

  async componentDidMount() {
    console.log('login gan');
  }

  render() {
    return <div>
        <section className="login container-fluid" id="login">
            <h2>Login</h2>
            <form action="">
                <p>
                    <label htmlFor="">Username</label> <br/>
                    <input type="text" value={this.state.username} onChange={(e) => this.setState({username: e.target.value})}/>
                </p>
                <p>
                    <label htmlFor="">Password</label> <br/>
                    <input type="password" value={this.state.password} onChange={(e) => this.setState({password: e.target.value})}/>
                </p>
                <div style={{
                  marginLeft: 'auto',
                  marginRight: 'auto',
                }}>
                  <ReCAPTCHA
                    sitekey="6LeVj74UAAAAAPFLq5-zHMyj-P0EsR8Yt2LoOOHC"
                    onChange={(key) => {
                      this.setState({
                        validated: !!key
                      });
                    }}
                  />
                </div>
                {this.props.store.isLoginBlocked && <p>Too much failed login attempt. Please try again in {this.props.store.blockedTimeRemaining} second</p>}
                <p>
                  <button className="btn-submit" onClick={(e) => {
                      this.props.history.push('forgot_password');
                  }} style={{
                    width: 150,
                    backgroundColor: 'white',
                    color: 'black',
                    marginRight: 16,
                    border: '1px solid #000'
                  }}>Forgot Password</button>

                  <button type="submit" className="btn-submit" style={{
                    width: 150
                  }} onClick={(e) => {
                    e.preventDefault();
                    if(!this.state.username) {
                      alert('Please input username');
                      return;
                    }
                    if(!this.state.password) {
                      alert('Please input password');
                      return;
                    }
                    if(!this.state.validated) {
                      alert('Please validate recaptcha');
                      return;
                    }
                    const result = this.props.store.login(this.state.username, this.state.password);
                    if(result) {
                      this.props.history.push('/');
                    }
                  }}>Login</button>

                </p>
            </form>
        </section>
    </div>;
  }
}

export {Login};
