import React from "react";
import { inject, observer } from "mobx-react";
import { DatePicker, Row, Col, Button, Table, Modal, Form, Input, notification } from 'antd';

const { confirm } = Modal;

@inject("store")
@observer
class Guestbook extends React.Component {

    state = {
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    async componentDidMount() {
      this.props.store.guestbook.get();
    }

  render() {
    const dataSource = [
      {
        key: '1',
        fullname: 'Hasta Ragil',
        email: 'hasta.ragil@gmail.com',
        message: 'Websitenya keren',
      },
      {
        key: '1',
        fullname: 'Aden Rahmandi',
        email: 'aden.rahmandi@gmail.com',
        message: 'Contentnya menarik',
      },
    ];

    const columns = [
      {
        title: 'Full Name',
        dataIndex: 'fullname',
        key: 'fullname',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'Message',
        dataIndex: 'message',
        key: 'message',
      },
      {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
        render: (_, row) => {
          return <div style={{
            display: 'flex',
            flexDirection: 'row',
          }}>
            <Button type="danger" onClick={() => {
              confirm({
                title: 'Do you want to delete the data?',
                onOk: () => {
                  this.props.store.guestbook.del(row.id).then(it => {
                    notification.success({
                      message: 'Delete data success'
                    });
                  });
                  console.log('OK');
                },
                onCancel: () => {
                  console.log('Cancel');
                },
              });

              // this.props.store.guestbook
            }}>Delete</Button>
          </div>
        }
      },
    ];

    return <div style={{
      width: '80vw',
      marginLeft: 'auto',
      marginRight: 'auto',
      display: 'flex',
      flexDirection: 'column',
      marginTop: 96
    }}>
      <div style={{
        marginTop: 16
      }}>
        <Table dataSource={this.props.store.guestbook.guestbooks} columns={columns} />
      </div>
    </div>;
  }
}

export {Guestbook}
