import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {HashLink as Link} from 'react-router-hash-link';

import {Homepage} from "./pages/homepage/Homepage";
import {Login} from "./pages/login/Login";
import {Store} from "./store";
import {Provider} from "mobx-react";
import {Header} from "./Header";
import {ForgotPassword} from "./pages/forgot_password/ForgotPassword";
import {ResetPassword} from "./pages/reset_password/ResetPassword";
import {Routes} from "./Routes";
import {Footer} from "./Footer";

const store = new Store();

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="container">

          <Header/>
          <div style={{
            paddingTop: 73,
            minHeight: 'calc(100vh - 38px)'
          }}>
            <Routes/>
            {/*<Switch>*/}
            {/*  <Route path="/" exact>*/}
            {/*    <Homepage/>*/}
            {/*  </Route>*/}
            {/*  <Route path="/login" exact>*/}
            {/*    <Login/>*/}
            {/*  </Route>*/}
            {/*  <Route path="/forgot_password" exact>*/}
            {/*    <ForgotPassword/>*/}
            {/*  </Route>*/}
            {/*  <Route path="/reset_password" exact>*/}
            {/*    <ResetPassword/>*/}
            {/*  </Route>*/}
            {/*</Switch>*/}
          </div>

          <Footer/>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
