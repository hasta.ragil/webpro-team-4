import React from "react";
import { inject, observer } from "mobx-react";
import {HashLink as Link} from "react-router-hash-link";
import {
  withRouter
} from 'react-router-dom';

@withRouter
@inject("store")
@observer
class Header extends React.Component {
    state = {
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    async componentDidMount() {
    }

    render() {
        return <header className="header-master" id={'banner'}>
        <div className="nav container-fluid">

          <a href="#banner" className="logo">
            cerdaskan
          </a>
          {this.props.store.userType === 'user' && <ul className="nav-list">
            <li><Link to="/#banner">Home</Link></li>
            <li><Link to="/#product">Product</Link></li>
            <li><Link to="/#our-team">Our Team</Link></li>
            <li><Link to="/#contact">Contact</Link></li>
            <li><Link to="/guestbook">Guest Book</Link></li>
            <li><Link to="/login">Login</Link></li>
          </ul>}
          {this.props.store.userType === 'admin' && <ul className="nav-list">
            <li><Link to="/">Home</Link></li>
            <li><Link to="/product_admin">Product Management</Link></li>
            <li><Link to="/guestbook">Guest Book</Link></li>
            <li><a onClick={(e) => {
              e.preventDefault();
              this.props.store.logout();
              this.props.history.push('/');
            }}>Logout</a></li>
          </ul>}
        </div>
      </header>;
    }
}

export {Header}
