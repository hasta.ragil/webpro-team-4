import React from "react";
import { inject, observer } from "mobx-react";

@inject("store")
@observer
class Footer extends React.Component {

    state = {
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    async componentDidMount() {
    }

    render() {
        return <footer>
          &copy; Cerdaskan 2019. All Rights Reserved - Visitor Count: {this.store.visitor_count.count}
        </footer>;
    }
}

export {Footer}
