import React from "react";
import { inject, observer } from "mobx-react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import {Homepage} from "./pages/homepage/Homepage";
import {Login} from "./pages/login/Login";
import {ForgotPassword} from "./pages/forgot_password/ForgotPassword";
import {ResetPassword} from "./pages/reset_password/ResetPassword";
import {HomepageAdmin} from "./pages/homepage_admin/HomepageAdmin";
import {ProductAdmin} from "./pages/product_admin/ProductAdmin";
import {Guestbook} from "./pages/guestbook/Guestbook";

@inject("store")
@observer
class Routes extends React.Component {

    state = {
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    async componentDidMount() {
    }

    render() {
        return <Switch>
          <Route path="/" exact>
            {this.props.store.userType === 'user' && <Homepage/>}
            {this.props.store.userType === 'admin' && <HomepageAdmin/>}
          </Route>
          <Route path="/product_admin" exact>
            <ProductAdmin/>
          </Route>
          <Route path="/guestbook" exact>
            <Guestbook/>
          </Route>
          <Route path="/login" exact>
            <Login/>
          </Route>
          <Route path="/forgot_password" exact>
            <ForgotPassword/>
          </Route>
          <Route path="/reset_password" exact>
            <ResetPassword/>
          </Route>
        </Switch>;
    }
}

export {Routes}
